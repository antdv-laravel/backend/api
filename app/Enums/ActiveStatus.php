<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class ActiveStatus extends Enum
{
  const STATUS_ACTIVE = "1";
  const STATUS_INACTIVE = "0";

  public static function values()
  {
    return [
      self::STATUS_ACTIVE => "Active",
      self::STATUS_INACTIVE => "In active",
    ];
  }

  public static function reverse()
  {
    return [
      "Active" => self::STATUS_ACTIVE,
      "In active" => self::STATUS_INACTIVE,
    ];
  }
}
