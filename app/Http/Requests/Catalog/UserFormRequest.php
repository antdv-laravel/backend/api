<?php

namespace App\Http\Requests\Catalog;

use App\Enums\ActiveStatus;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class UserFormRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    if ($this->isMethod("post")) {
      return $this->createRules();
    } elseif ($this->isMethod("put")) {
      return $this->updateRules();
    }
  }

  private function commonRules()
  {
    return [
      "fullname" => ["required", "string", "max:255"],
      "phone" => ["max:255"],
      "email" => ["max:255"],
      "is_active" => [new EnumValue(ActiveStatus::class)],
    ];
  }

  public function createRules()
  {
    return array_merge(
      [
        "username" => ["required", "string", "max:100", "unique:users"],
      ],
      self::commonRules()
    );
  }

  public function updateRules()
  {
    //    Log::info($this->route('user'));
    $rules = array_merge(
      [
        "username" => [
          "required",
          "string",
          "max:100",
          "unique:users,username," . $this->route("user")->id,
        ],
      ],
      self::commonRules()
    );
    return $rules;
  }
}
