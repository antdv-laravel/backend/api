<?php

namespace App\Http\Requests\Catalog;

use App\Enums\ActiveStatus;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class PartFormRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    if ($this->isMethod("post")) {
      return $this->createRules();
    } elseif ($this->isMethod("put")) {
      return $this->updateRules();
    }
  }

  private function commonRules()
  {
    return [
      "name" => ["string", "max:255"],
      "unit_id" => ["required", "integer"],
      "is_active" => [new EnumValue(ActiveStatus::class)],
    ];
  }

  public function createRules()
  {
    return array_merge(
      [
        "no" => ["required", "string", "max:255", "unique:parts"],
      ],
      self::commonRules()
    );
  }

  public function updateRules()
  {
    //    Log::info($this->route('part'));
    $rules = array_merge(
      [
        "no" => [
          "required",
          "string",
          "max:255",
          "unique:parts, no, " . $this->route("part")->id,
        ],
      ],
      self::commonRules()
    );
    return $rules;
  }
}
