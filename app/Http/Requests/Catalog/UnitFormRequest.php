<?php

namespace App\Http\Requests\Catalog;

use App\Enums\ActiveStatus;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class UnitFormRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    if ($this->isMethod("post")) {
      return $this->createRules();
    } elseif ($this->isMethod("put")) {
      return $this->updateRules();
    }
  }

  private function commonRules()
  {
    return [
      "description" => ["max:150"],
      "is_active" => [new EnumValue(ActiveStatus::class)],
    ];
  }

  public function createRules()
  {
    return array_merge(
      [
        "value" => ["required", "string", "max:10", "unique:units"],
      ],
      self::commonRules()
    );
  }

  public function updateRules()
  {
    //    Log::info($this->route('unit'));
    $rules = array_merge(
      [
        "value" => [
          "required",
          "string",
          "max:10",
          "unique:units,value," . $this->route("unit")->id,
        ],
      ],
      self::commonRules()
    );
    return $rules;
  }

  //  public function messages() {
  //    return
  //      $messages = [
  //        'value.required' => 'The value is required',
  //        'value.max' => 'The value may not be greater than 10 characters',
  //        'description.required' => 'The description is required',
  //        'is_active.required' => 'The is_active is required',
  //        'same' => 'The :attribute and :other must match.',
  //        'size' => 'The :attribute must be exactly :size.',
  //        'between' => 'The :attribute value :input is not between :min - :max.',
  //        'in' => 'The :attribute must be one of the following types: :values',
  //      ];
  //  }
}
