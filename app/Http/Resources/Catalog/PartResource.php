<?php

namespace App\Http\Resources\Catalog;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PartResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param Request $request
   *
   * @return array
   */
  public function toArray($request)
  {
    return [
      "id" => $this->id,
      "no" => $this->no,
      "name" => $this->name,
      "unit" => $this->unit,
      "description" => $this->description,
      "is_active" => $this->is_active,
      "creator" => $this->whenLoaded("createdBy"),
      "created" => $this->whenLoaded("createdBy", function () {
        return $this->created_at;
      }),
      "updater" => $this->whenLoaded("updatedBy"),
      "updated" => $this->whenLoaded("updatedBy", function () {
        return $this->updated_at;
      }),
    ];
  }
}
