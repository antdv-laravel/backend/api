<?php

namespace App\Http\Resources;

use App\Http\Resources\Catalog\WarehouseResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param Request $request
   *
   * @return array
   */
  public function toArray($request)
  {
    return [
      "id" => $this->id,
      "username" => $this->username,
      "fullname" => $this->fullname,
      "phone" => $this->phone,
      "email" => $this->email,
      "is_active" => $this->is_active,
      "main_page" => $this->main_page,
      "roles" => $this->getRoleNames(),
      "permissions" => $this->getAllPermissions(),
      "creator" => $this->whenLoaded("createdBy"),
      "created" => $this->whenLoaded("createdBy", function () {
        return $this->created_at;
      }),
      "updater" => $this->whenLoaded("updatedBy"),
      "updated" => $this->whenLoaded("updatedBy", function () {
        return $this->updated_at;
      }),
    ];
  }
}
