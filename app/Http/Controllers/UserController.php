<?php

namespace App\Http\Controllers;

use App\Http\Requests\Catalog\UserFormRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use ArrayObject;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
  function __construct()
  {
    $this->middleware(
      ["permission:user index | user show"],
      ["only" => ["index", "show"]]
    );
    $this->middleware(["permission:user create"], ["only" => ["store"]]);
    $this->middleware(["permission:user update"], ["only" => ["update"]]);
    $this->middleware(["permission:user delete"], ["only" => ["delete"]]);
  }

  public function index(Request $request)
  {
    $query = User::query()->with("warehouses");
    if ($request["all"] || $request["toXlsx"]) {
      $perPage = 9999999999;
    } else {
      $perPage = $request->get("pageSize");
    }
    if ($request["currentPage"]) {
      $this->page = $request->get("currentPage");
    }
    if ($request->has("username")) {
      $query->where("username", $request->get("username"));
    }
    if ($request->has("fullname")) {
      $query->where("fullname", $request->get("fullname"));
    }
    if ($request->has("phone")) {
      $query->where("phone", $request->get("phone"));
    }
    if ($request->has("email")) {
      $query->where("email", $request->get("email"));
    }
    if ($request->has("password")) {
      $query->where("password", $request->get("password"));
    }
    if ($request->has("sortField")) {
      if ($request->get("sortOrder") === "descend") {
        $query->orderByDesc($request->get("sortField"));
      } else {
        $query->orderBy($request->get("sortField"));
      }
    }
    return UserResource::collection(
      $query->paginate(
        $perPage,
        $columns = ["*"],
        $pageName = "page",
        $this->page
      )
    );
  }

  /**
   * Display the specified resource.
   *
   * @param User $user
   *
   * @return Response
   */
  public function show($id)
  {
    $user = User::where("id", $id)
      ->with(["createdBy", "updatedBy"])
      ->first();
    if (!$user) {
      return response()->json(
        [
          "errors" => [
            "message" => "User not found",
          ],
        ],
        Response::HTTP_BAD_REQUEST
      );
    }
    return new UserResource($user);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   *
   * @return Response
   */
  public function store(UserFormRequest $request)
  {
    $user = new User();
    DB::beginTransaction();
    try {
      // User Model
      $user->username = $request->username;
      $user->fullname = $request->fullname;
      $user->password = Hash::make($request->password);
      $user->phone = $request->phone;
      $user->email = $request->email;
      $user->main_page = $request->main_page;
      $user->is_active = $request->is_active;
      $user->created_by = $request->user()->id;
      if (!$user->save()) {
        return response()->json(
          [
            "errors" => [
              "message" => $user->getMessage(),
            ],
          ],
          Response::HTTP_NOT_ACCEPTABLE
        );
      }
      // UserWarehouse Model
      $userWarehouseVal = [];
      foreach ($request->user_warehouse as $userWarehouse) {
        $userWarehouseVal[] = [
          "user_id" => $user->id,
          "warehouse_id" => $userWarehouse,
          "is_main" => $request->main_warehouse === $userWarehouse ? "1" : null,
          "is_active" => "1",
          "created_by" => $request->user()->id,
          "updated_by" => $request->user()->id,
        ];
      }
      // ModelHasPermission Model
      $user->permissions()->sync($request->permissions);
    } catch (QueryException $e) {
      DB::rollBack();
      return response()->json(
        [
          "errors" => [
            "message" => $e->getMessage(),
          ],
        ],
        Response::HTTP_NOT_ACCEPTABLE
      );
    }
    DB::commit();
    return response()->json(
      ["message" => "Successfully created"],
      Response::HTTP_CREATED
    );
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param User $user
   *
   * @return Response
   */
  public function update(UserFormRequest $request, User $user)
  {
    if (!$user) {
      return response()->json(
        [
          "errors" => [
            "message" => "User not found",
          ],
        ],
        Response::HTTP_BAD_REQUEST
      );
    }
    DB::beginTransaction();
    try {
      // User Model
      $user->username = $request->username;
      $user->fullname = $request->fullname;
      if (
        $request->password !== null &&
        Hash::make($request->password) !== $user->password
      ) {
        $user->password = Hash::make($request->password);
      }
      $user->phone = $request->phone;
      $user->email = $request->email;
      $user->main_page = $request->main_page;
      $user->is_active = $request->is_active;
      $user->updated_by = $request->user()->id;
      if (!$user->save()) {
        return response()->json(
          [
            "errors" => [
              "message" => ["This username not found."],
            ],
          ],
          Response::HTTP_NOT_ACCEPTABLE
        );
      }
      // ModelHasPermission Model
      $user->permissions()->sync($request->permissions);
    } catch (QueryException $e) {
      DB::rollBack();
      return response()->json(
        [
          "errors" => [
            "message" => $e->getMessage(),
          ],
        ],
        Response::HTTP_NOT_ACCEPTABLE
      );
    }
    DB::commit();
    return response()->json(
      ["message" => "Successfully updated"],
      Response::HTTP_ACCEPTED
    );
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int\array $id
   *
   * @return Response
   */
  public function destroy($id)
  {
    $ids = json_decode($id, true);
    $users = User::whereIn("id", $ids)->get();
    if (!$users) {
      return response()->json(
        [
          "errors" => [
            "message" => "User not found",
          ],
        ],
        Response::HTTP_BAD_REQUEST
      );
    }
    try {
      User::destroy($ids);
    } catch (QueryException $e) {
      $getCode = $e->getCode();
      $errorInfo = $e->errorInfo;
      $getMessage = $e->getMessage();
      return response()->json(
        [
          "errors" => [
            "message" => $errorInfo,
          ],
        ],
        Response::HTTP_UNAVAILABLE_FOR_LEGAL_REASONS
      );
    }
    return response()->json(
      ["message" => "Successfully deleted"],
      Response::HTTP_NO_CONTENT
    );
  }

  /**
   * Display User`s routes.
   *
   * @param User $user
   *
   * @return Response
   */
  public function routes(User $user)
  {
    if (!$user) {
      return response()->json(
        [
          "errors" => [
            "message" => "User not found",
          ],
        ],
        Response::HTTP_BAD_REQUEST
      );
    }
    return response()->json(["code" => 0], Response::HTTP_OK);
    //    $routeList = [];
    $routeList["code"] = 0;
    $list = new ArrayObject([
      "router" => "root",
      "children" => [
        self::routeList(
          new ArrayObject([
            "router" => "adminGrp",
            "children" => ["userList"],
          ])
        ),
        self::routeList([
          "router" => "catalogGrp",
          "children" => [
            "unit",
            "part",
            "supplier",
            "warehouse",
            "documentType",
          ],
        ]),
      ],
    ]);
    $routeList["data"] = [$list];
    return response()->json([$routeList], Response::HTTP_OK);
  }

  public function routeList($routerArray)
  {
    return json_decode(json_encode($routerArray));
  }
}
