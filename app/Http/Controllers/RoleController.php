<?php

namespace App\Http\Controllers;

use App\Http\Resources\RoleResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  function __construct()
  {
    $this->middleware(
      ["permission:role list | role show"],
      ["only" => ["index", "show"]]
    );
    $this->middleware(
      ["permission:role create | role store"],
      ["only" => ["create", "store"]]
    );
    $this->middleware(
      ["permission:role edit | role update"],
      ["only" => ["edit", "update"]]
    );
    $this->middleware(["permission:role delete"], ["only" => ["destroy"]]);
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index(Request $request)
  {
    //    $query = Role::query()->orderBy('id', 'DESC');
    //    return RoleResource::collection($query->paginate($perPage));
    return RoleResource::collection(Role::all());
    //    $roles = Role::all();
    //    return response()->json(['roles' => $roles], Response::HTTP_OK);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    $permission = Permission::get();
    return view("roles.create", compact("permission"));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      "name" => "required|unique:roles,name",
      "permission" => "required",
    ]);
    //    $role = Role::create(['name' => $request->input('name')]);
    //    $role->syncPermissions($request->input('permission'));
    $role = new Role();
    $role->name = $request->name;
    $role->description = $request->description;
    $role->is_active = $request->is_active;
    $role->created_by = $request->user()->id;
    if (!$role->save()) {
      return response()->json(
        [
          "errors" => [
            "message" => "This username not found.",
          ],
        ],
        Response::HTTP_NOT_ACCEPTABLE
      );
    }
    $role->syncPermissions($request->input("permission"));
    return response()->json(
      ["message" => "Successfully created"],
      Response::HTTP_CREATED
    );
  }

  /**
   * Display the specified resource.
   *
   * @param Role $role
   *
   * @return Response
   */
  public function show(Role $role)
  {
    $rolePermissions = Permission::join(
      "role_has_permissions",
      "role_has_permissions.permission_id",
      "=",
      "permissions.id"
    )
      ->where("role_has_permissions.role_id", $role->id)
      ->get();
    return view("roles.show", compact("role", "rolePermissions"));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param Role $role
   *
   * @return Response
   */
  public function update(Request $request, Role $role)
  {
    $this->validate($request, [
      "name" => "required",
      "permission" => "required",
    ]);
    $role->name = $request->input("name");
    $role->save();
    $role->syncPermissions($request->input("permission"));
    return redirect()
      ->route("roles.index")
      ->with("success", "Role updated successfully");
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int\array $id
   *
   * @return Response
   */
  public function destroy($id)
  {
    DB::table("roles")
      ->where("id", $id)
      ->delete();
    return redirect()
      ->route("roles.index")
      ->with("success", "Role deleted successfully");
  }
}
