<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginFormRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
  /**
   * @param         $user
   * @param Request $request
   *
   * @return JsonResponse
   */
  protected function newToken($user, Request $request): JsonResponse
  {
    $token = $user->createToken($request->header("User-Agent"))->plainTextToken;
    return response()->json(
      [
        "message" => "Auth Successful",
        "user" => new UserResource($user),
        "token" => $token,
      ],
      Response::HTTP_OK
    );
  }

  public function signup(Request $request)
  {
    $request->validate([
      "username" => ["required", "string", "max:100", "unique:users"],
      "password" => ["required", "string", "min:3"],
      "fullname" => ["required", "string", "max:255"],
      "is_active" => ["required", "string", "min:1", "max:1"],
    ]);
    $user = User::create([
      "username" => $request->get("username"),
      "password" => Hash::make($request->get("password")),
      "fullname" => $request->get("fullname"),
      "is_active" => $request->get("is_active"),
    ]);
    return $this->newToken($user, $request);
  }

  public function login(LoginFormRequest $request)
  {
    $user = User::where("username", $request->username)->first();
    //    Log::info($user);
    if (!$user) {
      return response()->json(
        [
          "errors" => [
            "message" => "This username not found.",
          ],
        ],
        Response::HTTP_UNAUTHORIZED
      );
    }
    if (!Hash::check($request->password, $user->password)) {
      return response(
        [
          "errors" => [
            "message" => "Username and password do not match.",
          ],
        ],
        Response::HTTP_UNAUTHORIZED
      );
    }
    return $token = $this->newToken($user, $request);
  }

  public function logout(Request $request)
  {
    $request
      ->user()
      ->currentAccessToken()
      ->delete();
    return response()->json(
      [
        "message" => "Logout successful",
      ],
      Response::HTTP_OK
    );
  }
}
