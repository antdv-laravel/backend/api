<?php

namespace App\Http\Controllers\Api\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Requests\Catalog\UnitFormRequest;
use App\Http\Resources\Catalog\UnitResource;
use App\Models\Catalog\Unit;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UnitController extends Controller
{
  function __construct()
  {
    $this->middleware(
      ["permission:unit index | unit show"],
      ["only" => ["index", "show"]]
    );
    $this->middleware(["permission:unit create"], ["only" => ["store"]]);
    $this->middleware(["permission:unit update"], ["only" => ["update"]]);
    $this->middleware(["permission:unit delete"], ["only" => ["destroy"]]);
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index(Request $request)
  {
    $query = Unit::query();
    if ($request["all"] || $request["toXlsx"]) {
      $perPage = 9999999999;
    } else {
      $perPage = $request->get("pageSize");
    }
    if ($request["currentPage"]) {
      $this->page = $request->get("currentPage");
    }
    if ($request->has("value")) {
      $query->where("value", $request->get("value"));
    }
    if ($request->has("description")) {
      $query->where("description", $request->get("description"));
    }
    if ($request->has("sortField")) {
      if ($request->get("sortOrder") === "descend") {
        $query->orderByDesc($request->get("sortField"));
      } else {
        $query->orderBy($request->get("sortField"));
      }
    }
    return UnitResource::collection(
      $query->paginate(
        $perPage,
        $columns = ["*"],
        $pageName = "page",
        $this->page
      )
      //      $query->paginate()
    );
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   *
   * @return Response
   */
  public function store(UnitFormRequest $request)
  {
    $unit = new Unit();
    $unit->value = $request->value;
    $unit->description = $request->description;
    $unit->is_active = $request->is_active;
    $unit->created_by = $request->user()->id;
    if (!$unit->save()) {
      return response()->json(
        [
          "errors" => [
            "message" => "Unit not found.",
          ],
        ],
        Response::HTTP_NOT_ACCEPTABLE
      );
    }
    return response()->json(
      [
        "errors" => [
          "message" => "Successfully created",
        ],
      ],
      Response::HTTP_CREATED
    );
  }

  /**
   * Display the specified resource.
   *
   * @param Unit $unit
   *
   * @return Response
   */
  public function show($id)
  {
    $unit = Unit::where("id", $id)
      ->with(["createdBy", "updatedBy"])
      ->first();
    if (!$unit) {
      return response()->json(
        [
          "errors" => [
            "message" => "Unit not found",
          ],
        ],
        Response::HTTP_BAD_REQUEST
      );
    }
    return new UnitResource($unit);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param Unit $unit
   *
   * @return Response
   */
  public function update(UnitFormRequest $request, Unit $unit)
  {
    if (!$unit) {
      return response()->json(
        [
          "errors" => [
            "message" => "Unit not found",
          ],
        ],
        Response::HTTP_BAD_REQUEST
      );
    }
    $unit->value = $request->value;
    $unit->description = $request->description;
    $unit->is_active = $request->is_active;
    $unit->updated_by = $request->user()->id;
    if (!$unit->save()) {
      return response()->json(
        [
          "errors" => [
            "message" => $unit->getMessage(),
          ],
        ],
        Response::HTTP_NOT_ACCEPTABLE
      );
    }
    return response()->json(
      [
        "errors" => [
          "message" => "Successfully updated",
        ],
      ],
      Response::HTTP_ACCEPTED
    );
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int\array $id
   *
   * @return Response
   */
  public function destroy($id)
  {
    $ids = json_decode($id, true);
    $units = Unit::whereIn("id", $ids)->get();
    if (!$units) {
      return response()->json(
        [
          "errors" => [
            "message" => "Unit not found",
          ],
        ],
        Response::HTTP_BAD_REQUEST
      );
    }
    Unit::destroy($ids);
    return response()->json(
      ["message" => "Successfully deleted"],
      Response::HTTP_NO_CONTENT
    );
  }
}
