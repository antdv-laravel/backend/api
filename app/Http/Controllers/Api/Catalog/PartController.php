<?php

namespace App\Http\Controllers\Api\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Requests\Catalog\PartFormRequest;
use App\Http\Resources\Catalog\PartResource;
use App\Models\Catalog\Part;
use App\Models\Catalog\Unit;
use App\Models\Stock;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PartController extends Controller
{
  function __construct()
  {
    $this->middleware(
      ["permission:part index | part show"],
      ["only" => ["index", "show"]]
    );
    $this->middleware(["permission:part create"], ["only" => ["store"]]);
    $this->middleware(["permission:part update"], ["only" => ["update"]]);
    $this->middleware(["permission:part delete"], ["only" => ["destroy"]]);
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index(Request $request)
  {
    $query = Part::query()->with(["createdBy", "updatedBy"]);
    if ($request["all"] || $request["toXlsx"]) {
      $perPage = 99999999999999999999;
    } else {
      $perPage = $request->get("pageSize");
    }
    if ($request["currentPage"]) {
      $this->page = $request->get("currentPage");
    }
    if ($request->has("fromWhId")) {
      if ($request->get("fromWhId") > 0) {
        $stocksQuery = Stock::select("part_id")
          ->where("qty", ">", 0)
          ->where("warehouse_id", $request->get("fromWhId"));
        $stocks = $stocksQuery->get();
        $stockIds = [];
        foreach ($stocks as $stock) {
          $stockIds[] = $stock["part_id"];
        }
        $query->where(function ($q) use ($stockIds) {
          $q->whereIn("id", $stockIds);
        });
      }
    }
    if ($request->has("no")) {
      $query->where("no", $request->get("no"));
    }
    if ($request->has("description")) {
      $query->where("description", $request->get("description"));
    }
    if ($request->has("unit_id")) {
      $query->where("unit_id", $request->get("unit_id"));
    }
    if ($request->has("searchVal")) {
      $searchVal = $request->get("searchVal");
      $query->where(function ($q) use ($searchVal) {
        $q->whereRaw("no like '%" . $searchVal . "%'");
        $q->orWhereRaw("description like '%" . $searchVal . "%'");
      });
    }
    if ($request->has("sortField")) {
      if ($request->get("sortOrder") === "descend") {
        $query->orderByDesc($request->get("sortField"));
      } else {
        $query->orderBy($request->get("sortField"));
      }
    }
    return PartResource::collection(
      $query->paginate(
        $perPage,
        $columns = ["*"],
        $pageName = "page",
        $this->page
      )
    );
  }

  /**
   * Display the specified resource.
   *
   * @param Part $part
   *
   * @return Response
   */
  public function show($id)
  {
    $part = Part::where("id", $id)
      ->with(["createdBy", "updatedBy"])
      ->first();
    if (!$part) {
      return response()->json(
        [
          "errors" => [
            "message" => "Part not found",
          ],
        ],
        Response::HTTP_BAD_REQUEST
      );
    }
    return new PartResource($part);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   *
   * @return Response
   */
  public function store(PartFormRequest $request)
  {
    $unit = Unit::where("id", $request->unit_id)->first();
    if (!$unit) {
      return response()->json(
        [
          "errors" => [
            "message" => "Unit not found",
          ],
        ],
        Response::HTTP_BAD_REQUEST
      );
    }
    $part = new Part();
    $part->no = $request->no;
    $part->unit_id = $request->unit_id;
    $part->description = $request->description;
    $part->is_active = $request->is_active;
    $part->created_by = $request->user()->id;
    if (!$part->save()) {
      return response()->json(
        [
          "errors" => [
            "message" => $part->getMessage(),
          ],
        ],
        Response::HTTP_NOT_ACCEPTABLE
      );
    }
    return response()->json(
      ["message" => "Successfully created"],
      Response::HTTP_CREATED
    );
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param Part $part
   *
   * @return Response
   */
  public function update(PartFormRequest $request, Part $part)
  {
    $unit = Unit::where("id", $request->unit_id)->first();
    if (!$unit) {
      return response()->json(
        [
          "errors" => [
            "message" => "Unit not found",
          ],
        ],
        Response::HTTP_BAD_REQUEST
      );
    }
    if (!$part) {
      return response()->json(
        [
          "errors" => [
            "message" => "Part not found",
          ],
        ],
        Response::HTTP_BAD_REQUEST
      );
    }
    $part->no = $request->no;
    $part->unit_id = $unit->id;
    $part->description = $request->description;
    $part->is_active = $request->is_active;
    $part->updated_by = $request->user()->id;
    if (!$part->save()) {
      return response()->json(
        [
          "errors" => [
            "message" => $part->getMessage(),
          ],
        ],
        Response::HTTP_NOT_ACCEPTABLE
      );
    }
    return response()->json(
      ["message" => "Successfully updated"],
      Response::HTTP_ACCEPTED
    );
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int\array $id
   *
   * @return Response
   */
  public function destroy($id)
  {
    $ids = json_decode($id, true);
    $parts = Part::whereIn("id", $ids)->get();
    if (!$parts) {
      return response()->json(
        [
          "errors" => [
            "message" => "Part not found",
          ],
        ],
        Response::HTTP_BAD_REQUEST
      );
    }
    Part::destroy($ids);
    return response()->json(
      ["message" => "Successfully deleted"],
      Response::HTTP_NO_CONTENT
    );
  }
}
