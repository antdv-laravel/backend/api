<?php

namespace App\Models;

use App\Enums\ActiveStatus;
use App\Models\Catalog\Part;
use App\Models\Catalog\Supplier;
use App\Models\Catalog\Unit;
use App\Models\Catalog\Warehouse;
use App\Models\Document\Document;
use App\Models\Document\DocumentType;
use BenSampo\Enum\Traits\CastsEnums;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 *
 * @property int $id
 * @property string $username
 * @property string $fullname
 * @property string|null $phone
 * @property string|null $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $is_active
 * @property string|null $main_page
 * @property string|null $remember_token
 * @property int $created_by
 * @property int|null $updated_by
 * @property Carbon $created_at
 * @property Carbon|null $updated_at
 * @property Collection|Part[] $partCreators
 * @property Collection|Part[] $partUpdaters
 * @property Collection|Unit[] $unitCreators
 * @property Collection|Unit[] $unitUpdaters
 * @property Collection|User[] $createdBy
 * @property Collection|User[] $updatedBy
 * @package App\Models
 */
class User extends Authenticatable
{
  use HasApiTokens, HasFactory, Notifiable, CastsEnums, HasRoles;

  public $timestamps = false;
  protected $table = "users";

  protected $casts = [
    "is_active" => ActiveStatus::class,
    "email_verified_at" => "datetime",
    "created_by" => "int",
    "updated_by" => "int",
    "created_at" => "datetime",
    "updated_at" => "datetime",
  ];

  protected $dates = ["email_verified_at"];

  protected $hidden = ["password", "remember_token", "email_verified_at"];

  protected $fillable = [
    "username",
    "fullname",
    "phone",
    "email",
    "main_page",
    "is_active",
    "created_by",
    "created_at",
    "updated_by",
    "updated_at",
  ];

  public function createdBy()
  {
    return $this->belongsTo(User::class, "created_by");
  }

  public function updatedBy()
  {
    return $this->belongsTo(User::class, "updated_by");
  }

  public function unitCreators()
  {
    return $this->hasMany(Unit::class, "created_by");
  }

  public function unitUpdaters()
  {
    return $this->hasMany(Unit::class, "updated_by");
  }
}
