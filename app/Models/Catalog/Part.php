<?php

namespace App\Models\Catalog;

use App\Enums\ActiveStatus;
use App\Models\Document\DocumentDetail;
use App\Models\Stock;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Part
 *
 * @property int $id
 * @property string $no
 * @property string|null $name
 * @property int|null $unit_id
 * @property string|null $description
 * @property string|null $is_active
 * @property int $created_by
 * @property int|null $updated_by
 * @property Carbon $created_at
 * @property Carbon|null $updated_at
 * @property User|null $user
 * @property Unit|null $unit
 * @package App\Models\Catalog
 */
class Part extends Model
{
  use HasFactory;

  public $timestamps = false;
  protected $fillable = [
    "no",
    "name",
    "unit_id",
    "description",
    "is_active",
    "created_at",
    "updated_at",
    "created_by",
    "updated_by",
  ];
  protected $hidden = ["id", "created_at", "updated_at"];

  protected $casts = [
    "is_active" => ActiveStatus::class,
    "created_at" => "datetime",
    "updated_at" => "datetime",
  ];

  public function unit()
  {
    return $this->belongsTo(Unit::class, "unit_id");
  }

  public function createdBy()
  {
    return $this->belongsTo(User::class, "created_by");
  }

  public function updatedBy()
  {
    return $this->belongsTo(User::class, "updated_by");
  }
}
