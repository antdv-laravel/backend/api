<?php

namespace App\Models\Catalog;

use App\Enums\ActiveStatus;
use App\Models\User;
use BenSampo\Enum\Traits\CastsEnums;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Unit
 *
 * @property int $id
 * @property string $value
 * @property string|null $description
 * @property string|null $is_active
 * @property int $created_by
 * @property int|null $updated_by
 * @property Carbon $created_at
 * @property Carbon|null $updated_at
 * @property User|null $user
 * @property Collection|Part[] $parts
 * @package App\Models\Catalog
 */
class Unit extends Model
{
  use CastsEnums;

  public $timestamps = false;
  protected $fillable = [
    "value",
    "description",
    "is_active",
    "created_by",
    "created_at",
    "updated_by",
    "updated_at",
  ];

  protected $hidden = ["id", "created_at", "updated_at"];

  protected $casts = [
    "is_active" => ActiveStatus::class,
    "created_at" => "datetime",
    "updated_at" => "datetime",
  ];

  public function createdBy()
  {
    return $this->belongsTo(User::class, "created_by");
  }

  public function updatedBy()
  {
    return $this->belongsTo(User::class, "updated_by");
  }

  public function parts()
  {
    return $this->hasMany(Part::class);
  }
}
