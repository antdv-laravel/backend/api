<?php

namespace App\Helpers;

class UIHelper
{
  /**
   * Raqamni chiroyli ko`rinishda formatlash.
   *
   * @param float $number Formatlanishi kerak bo`lgan(Kiritilgan son-"KS") son. Odatda 0,<br>
   * @param int $decimal Formatlangan sonning butun qismi. Odatda 2 xona,<br>
   * @param string $decPoint Sonning butun va kasr qismini ajratib turuvchi belgi. Odatda '.'(nuqta),<br>
   * @param string $thousandsSep Minglikni ajratuvchi belgi. Odatda ' '(bo`sh joy),<br>
   * @param bool $removeZero 0(Nol)larni o`chirish. Odatda true,<br>
   * @param bool $showZero 0(Nol)larni ko`rsatish. Odatda true,<br>
   *                             Masalan:<br>
   *                             $fromatedNumber = UIHelper::numberFormat(123456.0098000);<br>
   *                             Natija: $fromatedNumber = 123 456.01;<br>
   *                             $fromatedNumber = UIHelper::numberFormat(123456.0098000,3,'.','`',false);<br>
   *                             Natija: $fromatedNumber = 123`456.010;<br>
   *                             $fromatedNumber = UIHelper::numberFormat(123456.0098000,3,'.','`',true,false);<br>
   *                             Natija: $fromatedNumber = 123`456.01;<br>
   *
   * @return string|null Natija string qaytadi.
   **/
  public static function numberFormat(
    float $number = 0,
    int $decimal = 2,
    string $decPoint = ".",
    string $thousandsSep = " ",
    bool $removeZero = true,
    bool $showZero = true
  ) {
    $formatedNumber = 0;
    if (empty($number) || $number === null) {
      $number = 0;
    }
    if ($showZero === true && $number === 0) {
      return $formatedNumber = 0;
    }
    if (!empty($number) || $number !== null || $number !== 0) {
      if (intval($number) === $number) {
        $formatedNumber = number_format(
          $number,
          $decimal,
          $decPoint,
          $thousandsSep
        );
      } else {
        $formatedNumber = number_format(
          $number,
          $decimal,
          $decPoint,
          $thousandsSep
        );
      }
    }
    $decVal = $number - (int) $number;
    if ($decVal === 0) {
      $formatedNumber = number_format($number, 0, $decPoint, $thousandsSep);
    } elseif ($removeZero == true && $decimal > 0) {
      $formatedNumber = rtrim($formatedNumber, 0);
      $formatedNumber = rtrim($formatedNumber, $decPoint);
    }
    return $formatedNumber;
  }

  /**
   * Array(massiv) ni string ga o`zgartirish
   *
   * @param array $arrayValue string ga aylantirilishi kerak bo`lgan array(massiv).
   * @param string $separator arrayning index(key)i bilan ajratib turuvchi string odatda ' '(bo`sh joy),<br>
   *                           Masalan:<br>
   *                           arrayToHtmlStringRecursive($array,' ');<br>
   *                           yoki<br>
   *                           arrayToHtmlStringRecursive($model->errors,' > ');<br>
   *
   * @return string|null Natija string qaytadi.
   **/
  public static function arrayToHtmlStringRecursive(
    array $arrayValue = [],
    string $separator = " "
  ) {
    $output = "";
    foreach ($arrayValue as $key => $av) {
      if (is_array($av)) {
        $output .=
          "<br><strong>" .
          $key .
          ": </strong>" .
          self::arrayToHtmlStringRecursive($av, $separator);
      } else {
        $output .= $separator . $av;
      }
    }
    return $output;
  }
}
