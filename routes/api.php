<?php
use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Catalog\PartController;
use App\Http\Controllers\Api\Catalog\UnitController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// public link
Route::post('login', [AuthController::class, 'login'])->name('login');
Route::post('signup', [AuthController::class, 'signup'])->name('signup');
// private link
Route::group(['middleware' => 'auth:sanctum'], function() {
//  Route::get('unit/{id}', [UnitController::class, 'show'])->name('api.units.show');
  Route::post('logout', [AuthController::class, 'logout'])->name('logout');
  Route::get('roles', function() {
    return ['data' => Role::where('guard_name', 'web')->select('id', 'name')->get()];
  });
  Route::get('permissions', function() {
    return ['data' => Permission::where('guard_name', 'web')->select('id', 'name')->get()];
  });
  Route::apiResource('users', UserController::class);
  Route::apiResource('units', UnitController::class);
  Route::apiResource('parts', PartController::class);

//  Route::get('block-status-photo-view', [BlockStatusController::class, 'blockStatusPhotoView'])->middleware("can:block-status photo-view");
//  Route::group(['middleware' => ['permission:document_list']], function () {
//    Route::apiResource('documents', DocumentController::class);
//  });
});
