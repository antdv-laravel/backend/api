<?php
return [
  /*
  |--------------------------------------------------------------------------
  | Cross-Origin Resource Sharing (CORS) Configuration
  |--------------------------------------------------------------------------
  |
  | Here you may configure your settings for cross-origin resource sharing
  | or "CORS". This determines what cross-origin operations may execute
  | in web browsers. You are free to adjust these settings as needed.
  |
  | To learn more: https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
  |
  */
//  'paths' => ['sanctum/csrf-cookie', 'api/*'],
  'paths' => ['api/*', 'login', 'logout'],
  'allowed_origins' => ['*'],
  'allowed_methods' => ['*'],
  'allowed_headers' => ['*'],
  'exposed_headers' => [],
  'allowed_origins_patterns' => [],
  'max_age' => 0,
  'supports_credentials' => true,

//  'allow_credentials' => false,
//  'allow_origins' => ['*'],
//  'allow_methods' => ['HEAD', 'GET', 'POST', 'PUT', 'PATCH', 'DELETE'], //, 'OPTIONS'
//  'allow_headers' => ['Content-Type', 'X-Auth-Token', 'Origin', 'Authorization',],
//  'expose_headers' => ['Cache-Control', 'Content-Language', 'Content-Type', 'Expires', 'Last-Modified'],

];
