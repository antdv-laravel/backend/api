<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("parts", function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->string("no", 50)->unique("uk-parts-part_no");
            $table->string("name")->nullable();
            $table
                ->unsignedInteger("unit_id")
                ->nullable()
                ->index("fk-parts-unit_id");
            $table->text("description")->nullable();
            $table
                ->enum("is_active", ["0", "1"])
                ->nullable()
                ->comment("0-Inactive; 1-Active");
            $table
                ->unsignedInteger("created_by")
                ->index("fk-parts-created_by")
                ->comment("Created user");
            $table
                ->unsignedInteger("updated_by")
                ->nullable()
                ->index("fk-parts-updated_by")
                ->comment("Updated user");
            $table
                ->dateTime("created_at")
                ->default(DB::raw("CURRENT_TIMESTAMP"))
                ->useCurrent()
                ->comment("Created time");
            $table
                ->dateTime("updated_at")
                ->default(DB::raw("NULL on update CURRENT_TIMESTAMP"))
                ->nullable()
                ->comment("Updated time");

            // add foreign keys
            $table
                ->foreign("unit_id", "fk-parts-unit_id")
                ->references("id")
                ->on("units")
                ->onUpdate("RESTRICT")
                ->onDelete("RESTRICT");
            $table
                ->foreign("created_by", "fk-parts-created_by")
                ->references("id")
                ->on("users")
                ->onUpdate("RESTRICT")
                ->onDelete("RESTRICT");
            $table
                ->foreign("updated_by", "fk-parts-updated_by")
                ->references("id")
                ->on("users")
                ->onUpdate("RESTRICT")
                ->onDelete("RESTRICT");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("parts", function (Blueprint $table) {
            $table->dropForeign("fk-parts-unit_id");
            $table->dropForeign("fk-parts-created_by");
            $table->dropForeign("fk-parts-updated_by");
        });
        Schema::dropIfExists("parts");
    }
}
