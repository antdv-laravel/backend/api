<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("units", function (Blueprint $table) {
            $table->increments("id");
            $table->string("value", 10)->unique("uk-units-value");
            $table->text("description")->nullable();
            $table
                ->enum("is_active", ["0", "1"])
                ->nullable()
                ->comment("0-Inactive; 1-Active");
            $table
                ->unsignedInteger("created_by")
                ->index("fk-units-created_by")
                ->comment("Created user");
            $table
                ->unsignedInteger("updated_by")
                ->nullable()
                ->index("fk-units-updated_by")
                ->comment("Updated user");
            $table
                ->dateTime("created_at")
                ->default(DB::raw("CURRENT_TIMESTAMP"))
                ->useCurrent()
                ->comment("Created time");
            $table
                ->dateTime("updated_at")
                ->default(DB::raw("NULL on update CURRENT_TIMESTAMP"))
                ->nullable()
                ->comment("Updated time");

            // add foreignKeys
            $table
                ->foreign("created_by", "fk-units-created_by")
                ->references("id")
                ->on("users")
                ->onUpdate("RESTRICT")
                ->onDelete("RESTRICT");
            $table
                ->foreign("updated_by", "fk-units-updated_by")
                ->references("id")
                ->on("users")
                ->onUpdate("RESTRICT")
                ->onDelete("RESTRICT");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("units", function (Blueprint $table) {
            $table->dropForeign("fk-units-created_by");
            $table->dropForeign("fk-units-updated_by");
        });
        Schema::dropIfExists("units");
    }
}
