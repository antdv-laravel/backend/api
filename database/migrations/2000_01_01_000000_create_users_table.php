<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("users", function (Blueprint $table) {
            $table->increments("id");
            $table->string("username", 100)->unique("uk-users-username");
            $table->string("fullname", 255);
            $table->string("phone", 255)->nullable();
            $table->string("email", 255)->nullable();
            $table->string("main_page", 255)->nullable();
            $table->timestamp("email_verified_at")->nullable();
            $table->string("password", 255);
            $table
                ->enum("is_active", ["0", "1"])
                ->nullable()
                ->comment("0-Inactive; 1-Active");
            $table->rememberToken();
            $table
                ->unsignedInteger("created_by")
                ->index("fk-users-created_by")
                ->comment("Created user");
            $table
                ->unsignedInteger("updated_by")
                ->nullable()
                ->index("fk-users-updated_by")
                ->comment("Updated user");
            $table
                ->dateTime("created_at")
                ->default(DB::raw("CURRENT_TIMESTAMP"))
                ->useCurrent()
                ->comment("Created time");
            $table
                ->dateTime("updated_at")
                ->default(DB::raw("NULL on update CURRENT_TIMESTAMP"))
                ->nullable()
                ->comment("Updated time");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("users");
    }
}
