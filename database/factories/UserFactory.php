<?php
namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserFactory extends Factory {

  /**
   * The name of the factory's corresponding model.
   *
   * @var string
   */
  protected $model = User::class;

  /**
   * Define the model's default state.
   *
   * @return array
   */
  public function definition() {
    return [
      'username' => $this->faker->unique()->userName,
      'fullname' => $this->faker->name,
      'email' => $this->faker->safeEmail,
      'phone' => $this->faker->phoneNumber,
      'email_verified_at' => now(),
      'is_active' => '1',
      'password' => Hash::make('123'),
      'remember_token' => Str::random(10),
      'created_by' => '1',
      'created_at' => now(),
      'updated_by' => '1',
    ];
  }

}
