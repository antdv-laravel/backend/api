<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Spatie\Permission\Traits\HasRoles;

class RolePermissionSeeder extends Seeder
{
  use HasRoles;

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Schema::disableForeignKeyConstraints();
    DB::table("roles")->truncate();
    DB::table("permissions")->truncate();
    DB::table("role_has_permissions")->truncate();
    app()[PermissionRegistrar::class]->forgetCachedPermissions();
    $this->createPermissions();
    $this->createRoles();
    Schema::enableForeignKeyConstraints();
  }

  public function createPermissions()
  {
    $guards = ["web"];
    $resources = ["user", "part", "unit"];
    $actions = ["index", "create", "update", "delete"];
    // Reset cached roles and permissions
    foreach ($resources as $resource) {
      foreach ($actions as $action) {
        $permission = null;
        foreach ($guards as $guard) {
          $tempPermission = $resource . " " . $action;
          Permission::create([
            "name" => $tempPermission,
            "guard_name" => $guard,
            "is_active" => "1",
            "created_by" => 1,
            "created_at" => now(),
          ]);
        } //guards
      } //$actions - each permission
    } //$resources
  }

  public function createRoles()
  {
    $roles = ["system", "super-admin", "admin"];
    $guards = ["web"];
    $permissions = Permission::all();
    foreach ($roles as $role) {
      foreach ($guards as $guard) {
        $createdRole = Role::create([
          "name" => $role,
          "guard_name" => $guard,
          "is_active" => "1",
          "created_by" => 1,
          "created_at" => now(),
        ]);
        foreach ($permissions as $permission) {
          $createdRole->givePermissionTo($permission->name);
        }
      } //guards
    } //$roles
  }
}
