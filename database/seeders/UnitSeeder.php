<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = [
            [
                "value" => "ea",
                "description" => "Dona",
                "is_active" => "1",
                "created_by" => 1,
                "created_at" => now(),
            ],
            [
                "value" => "l",
                "description" => "Litr",
                "is_active" => "1",
                "created_by" => 1,
                "created_at" => now(),
            ],
            [
                "value" => "m",
                "description" => "metr",
                "is_active" => "1",
                "created_by" => 1,
                "created_at" => now(),
            ],
            [
                "value" => "m²",
                "description" => "kv.metr",
                "is_active" => "1",
                "created_by" => 1,
                "created_at" => now(),
            ],
            [
                "value" => "m³",
                "description" => "kub.metr",
                "is_active" => "1",
                "created_by" => 1,
                "created_at" => now(),
            ],
            [
                "value" => "gr",
                "description" => "gramm",
                "is_active" => "1",
                "created_by" => 1,
                "created_at" => now(),
            ],
            [
                "value" => "kg",
                "description" => "kilogramm",
                "is_active" => "1",
                "created_by" => 1,
                "created_at" => now(),
            ],
            [
                "value" => "t",
                "description" => "Tonna",
                "is_active" => "1",
                "created_by" => 1,
                "created_at" => now(),
            ],
            [
                "value" => "pack",
                "description" => "Quti/коробка",
                "is_active" => "1",
                "created_by" => 1,
                "created_at" => now(),
            ],
        ];
        Schema::disableForeignKeyConstraints();
        DB::table("units")->truncate();
        DB::table("units")->insert($list);
        Schema::enableForeignKeyConstraints();
    }
}
