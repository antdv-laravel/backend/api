<?php
namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

class UserRolePermissionSeeder extends Seeder
{
  use HasRoles;

  /**
   * Run the database seeds.
   *
   * @return void`
   */
  public function run()
  {
    Schema::disableForeignKeyConstraints();
    DB::table("model_has_roles")->truncate();
    DB::table("model_has_permissions")->truncate();
    $this->createUserRoles();
    $this->createUserPermissions();
    Schema::enableForeignKeyConstraints();
  }

  public function createUserRoles()
  {
    $userList = ["system", "super", "admin"];
    $roles = Role::all();
    $users = User::whereIn("username", $userList)->get();
    foreach ($users as $user) {
      $user->assignRole($roles);
    }
  }

  public function createUserPermissions()
  {
    $userList = ["admin"];
    $users = User::whereIn("username", $userList)->get();
    $permissions = Permission::all();
    foreach ($users as $user) {
      $user->syncPermissions($permissions);
    }
  }
}
