<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminUserListEnd = [
            "main_page" => "user",
            "email_verified_at" => now(),
            "password" => Hash::make("1"),
            "is_active" => "1",
            "created_by" => 1,
            "created_at" => now(),
        ];
        $adminUserList = [
            array_merge(
                [
                    "username" => "system",
                    "fullname" => "System",
                    "phone" => null,
                ],
                $adminUserListEnd
            ),
            array_merge(
                [
                    "username" => "super",
                    "fullname" => "Super Admin",
                    "phone" => null,
                ],
                $adminUserListEnd
            ),
            array_merge(
                [
                    "username" => "admin",
                    "fullname" => "Admin",
                    "phone" => "+000(00)123-4567",
                ],
                $adminUserListEnd
            ),
        ];
        Schema::disableForeignKeyConstraints();
        DB::table("users")->truncate();
        DB::table("users")->insert($adminUserList);
        Schema::enableForeignKeyConstraints();
    }
}
