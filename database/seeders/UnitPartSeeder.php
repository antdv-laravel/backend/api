<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UnitPartSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        //units
        DB::statement("
         REPLACE INTO units ( value, description, is_active, created_by) VALUES
          ('ea', 'Dona', '1', 1),
          ('L', 'Litr', '1', 1),
          ('m', 'metr', '1', 1),
          ('m²', 'kv.metr', '1', 1),
          ('m³', 'kub.metr', '1', 1),
          ('gr', 'gramm', '1', 1),
          ('kg', 'kilogramm', '1', 1),
          ('t', 'Tonna', '1', 1),
          ('pack', 'Quti/коробка', '1', 1),
          ('l/m', 'Linear meter', '1', 1),
          ('kit', 'комплект', '1', 1),
          ('pair', 'juft', '1', 1),
          ('bag', 'qop', '1', 1),
          ('roll', 'rulon', '1', 1)
         ");
        sleep(3);
        //parts
        DB::statement("
         REPLACE INTO parts ( no, name, unit_id, description, is_active, created_by) VALUES
          ( 'Свароч.аппарат  220 V', 'Свароч.аппарат  220 V', (select id from units where value='EA' ), null, '1', 1),
          ( 'Болгарка 230', 'Болгарка 230', (select id from units where value='EA' ), null, '1', 1),
          ( 'Перфаратор', 'Перфаратор', (select id from units where value='EA' ), null, '1', 1),
          ( 'LED прожектор  200 Вт', 'LED прожектор  200 Вт', (select id from units where value='EA' ), null, '1', 1),
          ( 'Электр кабел 3х120', 'Электр кабел 3х120', (select id from units where value='m' ), null, '1', 1),
          ( 'Автомат 630А', 'Автомат 630А', (select id from units where value='EA' ), null, '1', 1),
          ( 'Эл.кабел провод 2х4', 'Эл.кабел провод 2х4', (select id from units where value='m' ), null, '1', 1),
          ( 'ПВС 4х6', 'ПВС 4х6', (select id from units where value='m' ), null, '1', 1),
          ( 'Брезент  ', 'Брезент  ', (select id from units where value='roll' ), null, '1', 1),
          ( 'Саморез 10', 'Саморез 10', (select id from units where value='kg' ), null, '1', 1),
          ( 'Маркер концелярский', 'Маркер концелярский', (select id from units where value='EA' ), null, '1', 1),
          ( 'Линейка', 'Линейка', (select id from units where value='EA' ), null, '1', 1),
          ( 'Резина (учиргич)', 'Резина (учиргич)', (select id from units where value='EA' ), null, '1', 1),
          ( 'Капалак кран 15д', 'Капалак кран 15д', (select id from units where value='EA' ), null, '1', 1),
          ( 'Трайник  20д', 'Трайник  20д', (select id from units where value='EA' ), null, '1', 1),
          ( 'Адаптр  20д', 'Адаптр  20д', (select id from units where value='EA' ), null, '1', 1),
          ( 'Трайник  16д', 'Трайник  16д', (select id from units where value='EA' ), null, '1', 1),
          ( 'Жгут', 'Жгут', (select id from units where value='EA' ), null, '1', 1),
          ( 'Тент эл. 1,5 m', 'Тент эл. 1,5 m', (select id from units where value='EA' ), null, '1', 1),
          ( 'Вешилка', 'Вешилка', (select id from units where value='EA' ), null, '1', 1),
          ( 'Акфа клей', 'Акфа клей', (select id from units where value='EA' ), null, '1', 1),
          ( 'Термоm', 'Термоm', (select id from units where value='EA' ), null, '1', 1),
          ( 'UPS  600 W', 'UPS  600 W', (select id from units where value='EA' ), null, '1', 1),
          ( 'П/эт плёнка ', 'П/эт плёнка ', (select id from units where value='kg' ), null, '1', 1),
          ( 'Емкост 1 tлик', 'Емкост 1 tлик', (select id from units where value='EA' ), null, '1', 1),
          ( 'Воздуш.фильт SAMSUNG эксковатор HFA 062', 'Воздуш.фильт SAMSUNG эксковатор HFA 062', (select id from units where value='EA' ), null, '1', 1),
          ( 'Закладной деталь 400х400х16мм', 'Закладной деталь 400х400х16мм', (select id from units where value='EA' ), null, '1', 1),
          ( 'Закладной деталь 160х240х4мм', 'Закладной деталь 160х240х4мм', (select id from units where value='EA' ), null, '1', 1),
          ( 'Закладной деталь 160х160х6мм', 'Закладной деталь 160х160х6мм', (select id from units where value='EA' ), null, '1', 1),
          ( 'Рохлер', 'Рохлер', (select id from units where value='EA' ), null, '1', 1)
         ");
        Schema::enableForeignKeyConstraints();
    }
}
