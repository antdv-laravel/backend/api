<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## About COREUI-backend

### installation step:
- git clone https://gitlab.com/nizomiddin/COREUI-backend.git
- composer install
- create DB  
- copy .env.example to .env (setting up DB & APP_URL)
- php artisan key:generate
- php artisan migrate
- php artisan db:seed

### good luck ☺☺☺ (login/pass: admin/1)


Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**

[nizomiddin@gmail.com](mailto:nizomiddin@gmail.com).

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

- 200: OK. The standard success code and default option.
- 201: Object created. Useful for the store actions.
- 204: No content. When an action was executed successfully, but there is no content to return.
- 206: Partial content. Useful when you have to return a paginated list of resources.
- 400: Bad request. The standard option for requests that fail to pass validation.
- 401: Unauthorized. The user needs to be authenticated.
- 403: Forbidden. The user is authenticated, but does not have the permissions to perform an action.
- 404: Not found. This will be returned automatically by Laravel when the resource is not found.
- 500: Internal server error. Ideally you're not going to be explicitly returning this, but if something unexpected breaks, this is what your user is going to receive.
- 503: Service unavailable. Pretty self explanatory, but also another code that is not going to be returned explicitly by the application.



```
add prettier(prettier.io): https://github.com/prettier/plugin-php
```
