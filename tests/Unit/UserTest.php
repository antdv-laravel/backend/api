<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;
//use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{



  /**
     * A basic unit test example.
     *
     * @return void
     */
  public function test_can_create_post() {

    $data = [
      'title' => $this->faker->sentence,
      'content' => $this->faker->paragraph,
    ];

    $this->post(route('posts.store'), $data)
         ->assertStatus(201)
         ->assertJson($data);
  }

}
